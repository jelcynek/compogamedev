package
{
	public final class Resource
	{
		[Embed(source = "../img/comb.png")]		public static const COMB_IMG		: Class;
		[Embed(source = "../img/mustache.png")]	public static const MUSTACHE_IMG	: Class;
		[Embed(source = "../img/hand1.png")]	public static const HAND1_IMG		: Class;
		[Embed(source = "../img/hand2.png")]	public static const HAND2_IMG		: Class;
		
		[Embed(source = "../img/p1_1.png")]		public static const P11_IMG			: Class;
		[Embed(source = "../img/p1_2.png")]		public static const P12_IMG			: Class;
		[Embed(source = "../img/p1_3.png")]		public static const P13_IMG			: Class;
		[Embed(source = "../img/p1_4.png")]		public static const P14_IMG			: Class;
		[Embed(source = "../img/p2_1.png")]		public static const P21_IMG			: Class;
		[Embed(source = "../img/p2_2.png")]		public static const P22_IMG			: Class;
		[Embed(source = "../img/p2_3.png")]		public static const P23_IMG			: Class;
		[Embed(source = "../img/p2_4.png")]		public static const P24_IMG			: Class;
		[Embed(source = "../img/p3_1.png")]		public static const P31_IMG			: Class;
		[Embed(source = "../img/p3_2.png")]		public static const P32_IMG			: Class;
		[Embed(source = "../img/p3_3.png")]		public static const P33_IMG			: Class;
		[Embed(source = "../img/p3_4.png")]		public static const P34_IMG			: Class;
		[Embed(source = "../img/p4_1.png")]		public static const P41_IMG			: Class;
		[Embed(source = "../img/p4_2.png")]		public static const P42_IMG			: Class;
		[Embed(source = "../img/p4_3.png")]		public static const P43_IMG			: Class;
		[Embed(source = "../img/p4_4.png")]		public static const P44_IMG			: Class;
		
		[Embed(source = "../img/button1_off.png")]			public static const B1_ON	: Class;
		[Embed(source = "../img/button1_on.png")]			public static const B1_OFF	: Class;
		[Embed(source = "../img/button2_off.png")]			public static const B2_ON	: Class;
		[Embed(source = "../img/button2_on.png")]			public static const B2_OFF	: Class;				
		[Embed(source = "../img/buttondown_off.png")]		public static const BD_ON	: Class;
		[Embed(source = "../img/buttondown_on.png")]		public static const BD_OFF	: Class;
		[Embed(source = "../img/buttonup_off.png")]			public static const BU_ON	: Class;
		[Embed(source = "../img/buttonup_on.png")]			public static const BU_OFF	: Class;		
		[Embed(source = "../img/buttonleft_off.png")]		public static const BL_ON	: Class;
		[Embed(source = "../img/buttonleft_on.png")]		public static const BL_OFF	: Class;
		[Embed(source = "../img/buttonright_off.png")]		public static const BR_ON	: Class;
		[Embed(source = "../img/buttonright_on.png")]		public static const BR_OFF	: Class;	
		
		[Embed(source = "../img/dust_h11.png")]		public static const DUST_H11	: Class;
		[Embed(source = "../img/dust_h12.png")]		public static const DUST_H12	: Class;
		[Embed(source = "../img/dust_h21.png")]		public static const DUST_H21	: Class;
		[Embed(source = "../img/dust_h22.png")]		public static const DUST_H22	: Class;
		[Embed(source = "../img/dust_v.png")]		public static const DUST_V		: Class;
		[Embed(source = "../img/dust_l1.png")]		public static const DUST_L1		: Class;
		[Embed(source = "../img/dust_l2.png")]		public static const DUST_L2		: Class;
		[Embed(source = "../img/dust_r1.png")]		public static const DUST_R1		: Class;
		[Embed(source = "../img/dust_r2.png")]		public static const DUST_R2		: Class;
		//Example on how to embed images.
		//[Embed(source = '/images/buttons/attack.png')]							
	}
}