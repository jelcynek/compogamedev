package  
{
	/**
	 * ...
	 * @author jjj
	 */
	public class MustacheEnergy
	{
		public static const maxEnergy:Number = 1000;
		
		public var partH1:Number = maxEnergy;
		public var partH2:Number = maxEnergy;
		public var partV:Number = maxEnergy;
		public var partL:Number = maxEnergy;
		public var partR:Number = maxEnergy;
		
		public function MustacheEnergy() 
		{

		}
		
	}

}