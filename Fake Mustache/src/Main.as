package 
{
	import flash.automation.MouseAutomationAction;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author jelcynek
	 */
	public class Main extends Sprite 
	{
		const MUSTACHE_POSITION_X:int = 90;
		const MUSTACHE_POSITION_Y:int = 125;
		const COMBH_POSITION_X1:int = 125;
		const COMBH_POSITION_X2:int = 225;
		const COMBH_SPOSITION_Y:int = 100;
		const COMBH_EPOSITION_Y:int = 130;
		const COMBV_SPOSITION_X:int = 160;
		const COMBV_EPOSITION_X:int = 320;
		const HAND_POSITION_X1:int = 130;
		const HAND_POSITION_X2:int = 300;
		const HAND_POSITION_Y:int = 85;
		const BUTTONL_POSITION_X:int = 172;
		const BUTTONL_POSITION_Y:int = 60;
		const BUTTONR_POSITION_X:int = 218;
		const BUTTONR_POSITION_Y:int = 60;
		const BUTTONU_POSITION_X:int = 55;
		const BUTTONU_POSITION_Y:int = 105;
		const BUTTOND_POSITION_X:int = 55;
		const BUTTOND_POSITION_Y:int = 150;
		const BUTTON1_POSITION_X:int = 5;
		const BUTTON1_POSITION_Y:int = 120;
		const BUTTON2_POSITION_X:int = 355;
		const BUTTON2_POSITION_Y:int = 120;
		const SPEED_X:int = 200;
		const SPEED_Y:int = 100;
		const ENERGY_SPEEDX:int = 300;
		const ENERGY_SPEEDY:int = 150;
		const ENERGY_SPEEDB:int = 30;
 		const MS_PER_FRAME:Number = 33.3;
		const BACKGROUNDCHANGE_MS:Number = 800;
		const BUTTONALTER_MS:Number = 300;	 
		
		const PLAYER_COLORS:Array = [ 0x616262, 0x27ADE3, 0xEE368A, 0xB0D136 ];
		
		var player1Background:MovieClip = new MovieClip();
		var player2Background:MovieClip = new MovieClip();
		var player3Background:MovieClip = new MovieClip();
		var player4Background:MovieClip = new MovieClip();
		var playersBackgrounds:Array = new Array(4);
		
		var player1Mustache:MovieClip = new MovieClip();
		var player2Mustache:MovieClip = new MovieClip();
		var player3Mustache:MovieClip = new MovieClip();
		var player4Mustache:MovieClip = new MovieClip();
		var mustaches:Array = new Array(4);
	
		var player1Comb:MovieClip = new MovieClip();
		var player2Comb:MovieClip = new MovieClip();
		var player3Comb:MovieClip = new MovieClip();
		var player4Comb:MovieClip = new MovieClip();
		var combs:Array = new Array(4);
	
		var player1Hand:MovieClip = new MovieClip();
		var player2Hand:MovieClip = new MovieClip();
		var player3Hand:MovieClip = new MovieClip();
		var player4Hand:MovieClip = new MovieClip();
		var hands:Array = new Array(4);
		
		var player1Button1:MovieClip = new MovieClip();
		var player1Button2:MovieClip = new MovieClip();
		var player2Button1:MovieClip = new MovieClip();
		var player2Button2:MovieClip = new MovieClip();
		var player3Button1:MovieClip = new MovieClip();
		var player3Button2:MovieClip = new MovieClip();
		var player4Button1:MovieClip = new MovieClip();
		var player4Button2:MovieClip = new MovieClip();
		var buttons1:Array = new Array(4);
		var buttons2:Array = new Array(4);
		
		var player1Dusts:MovieClip = new MovieClip();
		var player2Dusts:MovieClip = new MovieClip();
		var player3Dusts:MovieClip = new MovieClip();
		var player4Dusts:MovieClip = new MovieClip();
		var dusts:Array = new Array(4);
		
		var currentBackground:Array = [0, 0, 0, 0];
		
		var directions:Array = [0, 0, 0, 0];
		
		var backgroundChangeCounter:Number = 0;
		var buttonAlterCounter:Number = 0;
		
		var prevTime:int = 0;
		var scores:Array = [0, 0, 0, 0];
		
		var MustachesEnergys:Array = new Array(4);
		
		var ended:Boolean = false;
		
		private var endTimer:Timer = new Timer(55 * 1000);
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			ChainJam.init();
			
			InitializeBackgrounds();
			InitializeMustaches();
			InitializeCombs();
			InitializeHands();
			InitializeButtons();
			InitializeEnergys();
			InitializeDusts();
			
			RandomizePlayers();
			loadImages();
			AddingChildsToScene();
			
			addEventListener(Event.ENTER_FRAME, onEnter);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			
			endTimer.addEventListener(TimerEvent.TIMER, onEndTimer);
			endTimer.start();
			backgroundChange();
		}
		
		private function onEndTimer(event:TimerEvent):void
		{
			endGame();
		}
		
		private function AddingChildsToScene():void
		{	
			addChild(player1Background);
			addChild(player2Background);
			addChild(player3Background);
			addChild(player4Background);	
			addChild(player1Mustache);
			addChild(player2Mustache);
			addChild(player3Mustache);
			addChild(player4Mustache);
			addChild(player1Dusts);
			addChild(player2Dusts);
			addChild(player3Dusts);
			addChild(player4Dusts);
			addChild(player1Comb);
			addChild(player2Comb);
			addChild(player3Comb);
			addChild(player4Comb);
			addChild(player1Hand);
			addChild(player2Hand);
			addChild(player3Hand);
			addChild(player4Hand);
			addChild(player1Button1);
			addChild(player1Button2);
			addChild(player2Button1);
			addChild(player2Button2);
			addChild(player3Button1);
			addChild(player3Button2);
			addChild(player4Button1);
			addChild(player4Button2);
		}
		
		private function InitializeDusts():void
		{
			dusts = [player1Dusts, player2Dusts, player3Dusts, player4Dusts];
			player1Dusts.x = 0;
			player1Dusts.y = 0;
			player2Dusts.x = 400;
			player2Dusts.y = 0;
			player3Dusts.x = 0;
			player3Dusts.y = 225;
			player4Dusts.x = 400;
			player4Dusts.y = 225;
		}
		
		private function InitializeEnergys():void
		{
			MustachesEnergys = [new MustacheEnergy(), new MustacheEnergy(), new MustacheEnergy(), new MustacheEnergy()];
		}
		
		private function InitializeBackgrounds():void
		{
			playersBackgrounds = [player1Background, player2Background, player3Background, player4Background];
			player1Background.x = 0;
			player1Background.y = 0;
			player2Background.x = 400;
			player2Background.y = 0;
			player3Background.x = 0;
			player3Background.y = 225;
			player4Background.x = 400;
			player4Background.y = 225;
		}
		
		private function InitializeMustaches():void
		{
			mustaches = [player1Mustache, player2Mustache, player3Mustache, player4Mustache];
			player1Mustache.x = 0;
			player1Mustache.y = 0;
			player2Mustache.x = 400;
			player2Mustache.y = 0;
			player3Mustache.x = 0;
			player3Mustache.y = 225;
			player4Mustache.x = 400;
			player4Mustache.y = 225;
		}
		
		private function InitializeCombs():void
		{
			combs = [player1Comb, player2Comb, player3Comb, player4Comb];
			player1Comb.x = 0;
			player1Comb.y = 0;
			player2Comb.x = 400;
			player2Comb.y = 0;
			player3Comb.x = 0;
			player3Comb.y = 225;
			player4Comb.x = 400;
			player4Comb.y = 225;
		}
		
		private function InitializeHands():void
		{
			hands = [player1Hand, player2Hand, player3Hand, player4Hand];
			player1Hand.x = 0;
			player1Hand.y = 0;
			player2Hand.x = 400;
			player2Hand.y = 0;
			player3Hand.x = 0;
			player3Hand.y = 225;
			player4Hand.x = 400;
			player4Hand.y = 225;	
		}
		
		private function InitializeButtons():void
		{
			buttons1 = [player1Button1, player2Button1, player3Button1, player4Button1];
			buttons2 = [player1Button2, player2Button2, player3Button2, player4Button2];
			player1Button1.x = player1Button2.x = 0;
			player1Button1.y = player1Button2.y = 0;
			player2Button1.x = player2Button2.x = 400;
			player2Button1.y = player2Button2.y = 0;
			player3Button1.x = player3Button2.x = 0;
			player3Button1.y = player3Button2.y = 225;
			player4Button1.x = player4Button2.x = 400;
			player4Button1.y = player4Button2.y = 225;
		}
		
		private function RandomizePlayers():void
		{
			var usedNumbers:Array = new Array();
			var myNum:Number;
			for (var i:int = 0; i < 4; i++)
			{
				do 
				{
					myNum = Math.floor(Math.random()*4);
				} while (usedNumbers.indexOf(myNum) != -1);
				loadBackground(playersBackgrounds[i], myNum);
				Tint(playersBackgrounds[i], PLAYER_COLORS[i]);
				usedNumbers.push(myNum);
				trace ("Player " + (i+1) + ": background " + myNum);
			}
		}
		
		private function loadBackground(background:MovieClip, backgroundNumber:int):void
		{
			if (backgroundNumber == 0)
			{
				background.addChild(new Resource.P11_IMG());
				background.addChild(new Resource.P12_IMG());
				background.addChild(new Resource.P13_IMG());
				background.addChild(new Resource.P14_IMG());
			}
			else if (backgroundNumber == 1)
			{
				background.addChild(new Resource.P21_IMG());
				background.addChild(new Resource.P22_IMG());
				background.addChild(new Resource.P23_IMG());
				background.addChild(new Resource.P24_IMG());
			}
			else if (backgroundNumber == 2)
			{
				background.addChild(new Resource.P31_IMG());
				background.addChild(new Resource.P32_IMG());
				background.addChild(new Resource.P33_IMG());
				background.addChild(new Resource.P34_IMG());
			}
			else if (backgroundNumber == 3)
			{
				background.addChild(new Resource.P41_IMG());
				background.addChild(new Resource.P42_IMG());
				background.addChild(new Resource.P43_IMG());
				background.addChild(new Resource.P44_IMG());
			}
			else
			{
				trace("WTF!");
			}
		}
		
		public function hideAllChilds(movieClip:MovieClip):void
		{
				for (var i:int = 0; i < movieClip.numChildren; i++)
				{
					movieClip.getChildAt(i).visible = false;
				}
		}
		
		public function loadImages():void
		{
			for each(var mustache:MovieClip in mustaches)
			{
				var mustacheBitmap:Bitmap = new Resource.MUSTACHE_IMG(); 
				mustacheBitmap.x = MUSTACHE_POSITION_X;
				mustacheBitmap.y = MUSTACHE_POSITION_Y;
				mustache.addChild(mustacheBitmap)
			}
			
			for each (var dust:MovieClip in dusts)
			{
					var dustBitmap:Bitmap = new Resource.DUST_H11();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
					dustBitmap = new Resource.DUST_H12();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
					dustBitmap = new Resource.DUST_H21();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
					dustBitmap = new Resource.DUST_H22();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
					dustBitmap = new Resource.DUST_V();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
					dustBitmap = new Resource.DUST_L1();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
					dustBitmap = new Resource.DUST_L2();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
					dustBitmap = new Resource.DUST_R1();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
					dustBitmap = new Resource.DUST_R2();
					dustBitmap.x = MUSTACHE_POSITION_X;
					dustBitmap.y = MUSTACHE_POSITION_Y;
					dust.addChild(dustBitmap);
			}
			
			for each(var comb:MovieClip in combs)
			{
				var combBitmap:Bitmap = new Resource.COMB_IMG();
				combBitmap.x = COMBH_POSITION_X1;
				combBitmap.y = COMBH_SPOSITION_Y;
				comb.addChild(combBitmap);
			}
					
			for each(var hand:MovieClip in hands)
			{
				var hand1Bitmap:Bitmap = new Resource.HAND1_IMG();
				var hand2Bitmap:Bitmap = new Resource.HAND2_IMG();
				hand1Bitmap.x = HAND_POSITION_X1;
				hand1Bitmap.y = HAND_POSITION_Y;
				hand1Bitmap.visible = false;
				hand2Bitmap.x = HAND_POSITION_X1;
				hand2Bitmap.y = HAND_POSITION_Y;
				hand2Bitmap.visible = false;
				hand.addChild(hand1Bitmap);
				hand.addChild(hand2Bitmap);
			}
								
			for each(var button:MovieClip in buttons1)
			{
				var button1On_Bitmap:Bitmap = new Resource.B1_ON();
				var button1Off_Bitmap:Bitmap = new Resource.B1_OFF();
				var buttonUpOn_Bitmap:Bitmap = new Resource.BU_ON();
				var buttonUpOff_Bitmap:Bitmap = new Resource.BU_OFF();
				var buttonLeftOn_Bitmap:Bitmap = new Resource.BL_ON();
				var buttonLeftOff_Bitmap:Bitmap = new Resource.BL_OFF();
				
				button1On_Bitmap.x = button1Off_Bitmap.x = BUTTON1_POSITION_X;
				button1On_Bitmap.y = button1Off_Bitmap.y = BUTTON1_POSITION_Y;
				button1On_Bitmap.visible = button1Off_Bitmap.visible = false;
				buttonUpOn_Bitmap.x = buttonUpOff_Bitmap.x = BUTTONU_POSITION_X;
				buttonUpOn_Bitmap.y = buttonUpOff_Bitmap.y = BUTTONU_POSITION_Y;
				buttonUpOff_Bitmap.visible = false;
				buttonLeftOn_Bitmap.x = buttonLeftOff_Bitmap.x = BUTTONL_POSITION_X;
				buttonLeftOn_Bitmap.y = buttonLeftOff_Bitmap.y = BUTTONL_POSITION_Y;
				buttonLeftOff_Bitmap.visible = buttonLeftOn_Bitmap.visible = false;
				button.addChild(button1On_Bitmap);
				button.addChild(button1Off_Bitmap);
				button.addChild(buttonUpOn_Bitmap);
				button.addChild(buttonUpOff_Bitmap);
				button.addChild(buttonLeftOn_Bitmap);
				button.addChild(buttonLeftOff_Bitmap);
			}
			
			for each(var button:MovieClip in buttons2)
			{
				var button2On_Bitmap:Bitmap = new Resource.B2_ON();
				var button2Off_Bitmap:Bitmap = new Resource.B2_OFF();
				var buttonDownOn_Bitmap:Bitmap = new Resource.BD_ON();
				var buttonDownOff_Bitmap:Bitmap = new Resource.BD_OFF();
				var buttonRightOn_Bitmap:Bitmap = new Resource.BR_ON();
				var buttonRightOff_Bitmap:Bitmap = new Resource.BR_OFF();
				
				button2On_Bitmap.x = button2Off_Bitmap.x = BUTTON2_POSITION_X;
				button2On_Bitmap.y = button2Off_Bitmap.y = BUTTON2_POSITION_Y;
				button2On_Bitmap.visible = button2Off_Bitmap.visible = false;
				buttonDownOn_Bitmap.x = buttonDownOff_Bitmap.x = BUTTOND_POSITION_X;
				buttonDownOn_Bitmap.y = buttonDownOff_Bitmap.y = BUTTOND_POSITION_Y;
				buttonRightOn_Bitmap.x = buttonRightOff_Bitmap.x = BUTTONR_POSITION_X;
				buttonRightOn_Bitmap.y = buttonRightOff_Bitmap.y = BUTTONR_POSITION_Y;
				buttonRightOn_Bitmap.visible = buttonRightOff_Bitmap.visible = false;
				
				button.addChild(button2On_Bitmap);
				button.addChild(button2Off_Bitmap);
				button.addChild(buttonDownOn_Bitmap);
				button.addChild(buttonDownOff_Bitmap);
				button.addChild(buttonRightOn_Bitmap);
				button.addChild(buttonRightOff_Bitmap);
			}
		}
		
		private function Tint(display:MovieClip, color:uint) 
		{
			var R:Number = ((color >> 16) & 0x0000FF) / 255;
			var G:Number = ((color >> 8) & 0x0000FF) / 255;
			var B:Number = (color & 0x0000FF) / 255;
			display.transform.colorTransform = new ColorTransform(R, G, B);
		}
		
		private function backgroundChange():void
		{
			trace("background change");
			
			for (var i:int = 0; i < 4; i++)
			{		
				hideAllChilds(playersBackgrounds[i]);
				
				do {
					var myNum:int = Math.floor(Math.random() * 4);
				} while (currentBackground[i] == myNum);
				
				playersBackgrounds[i].getChildAt(myNum).visible = true;
				currentBackground[i] = myNum;
			}
		}
		
		private function handAnimate(i:int):void
		{
			if (hands[i].getChildAt(0).visible == true)
				{
					hands[i].getChildAt(0).visible = false;
					hands[i].getChildAt(1).visible = true;
				}
				else
				{
					hands[i].getChildAt(0).visible = true;
					hands[i].getChildAt(1).visible = false;
				}
		}
		
		private function ButtonAlter()
		{
			for (var i:int = 0; i < 4; i++)
			{
				var energy:MustacheEnergy = MustachesEnergys[i];
				
				if (getPhase(i) <= 1)
				{
					buttonAnimate(3, buttons1[i]);
					buttonAnimate(3, buttons2[i]);
				}
				else if (getPhase(i) == 2)
				{
					buttonAnimate(5, buttons1[i]);
					buttonAnimate(5, buttons2[i]);
				}
			}
		}
		
		private function buttonAnimate(i:int, buttonsMovieclip:MovieClip):void
		{
			buttonsMovieclip.getChildAt(i).visible = !buttonsMovieclip.getChildAt(i).visible;
		}
		
		private function onEnter(e:Event):void
		{	
			var elapsedTime:int = getTimer() - prevTime;
			prevTime += elapsedTime;
			
			backgroundChangeCounter += elapsedTime;
			
			if (backgroundChangeCounter > BACKGROUNDCHANGE_MS)
			{
				backgroundChangeCounter = 0;
				backgroundChange();
			}
			
			buttonAlterCounter += MS_PER_FRAME;
			if (buttonAlterCounter > BUTTONALTER_MS)
			{
				buttonAlterCounter = 0;
				ButtonAlter();
			}		
			
			for (var i:int = 0; i < 4; i++)
			{
				updatePlayer(i, elapsedTime);
			}
			updateScores();
		}
		
		private function onKeyDown(event:KeyboardEvent):void
		{
			// PLAYER1
			if (event.keyCode == ChainJam.PLAYER1_UP)
			{
				if (getPhase(0) > 1)
					return;
					
				directions[0] = -1;
			}
			else if (event.keyCode == ChainJam.PLAYER1_DOWN)
			{
				if (getPhase(0) > 1)
					return;
				
				directions[0] = 1;
			}
			else if (event.keyCode == ChainJam.PLAYER1_LEFT)
			{
				if (getPhase(0) != 2)
					return;
					
				directions[0] = -1;
			}
			else if (event.keyCode == ChainJam.PLAYER1_RIGHT)
			{
				if (getPhase(0) != 2)
					return;
					
				directions[0] = 1;
			}
			else if (event.keyCode == ChainJam.PLAYER1_ACTION1)
			{
				if (getPhase(0) != 3)
					return;
					
				actionButton(0, 0);
			}
			else if (event.keyCode == ChainJam.PLAYER1_ACTION2)
			{
				if (getPhase(0) != 4)
					return;
					
				actionButton(1, 0);
			}
			// PLAYER2
			else if (event.keyCode == ChainJam.PLAYER2_UP)
			{
				if (getPhase(1) > 1)
					return;
					
				directions[1] = -1;
			}
			else if (event.keyCode == ChainJam.PLAYER2_DOWN)
			{
				if (getPhase(1) > 1)
					return;
				
				directions[1] = 1;
			}
			else if (event.keyCode == ChainJam.PLAYER2_LEFT)
			{
				if (getPhase(1) != 2)
					return;
					
				directions[1] = -1;
			}
			else if (event.keyCode == ChainJam.PLAYER2_RIGHT)
			{
				if (getPhase(1) != 2)
					return;
					
				directions[1] = 1;
			}
			else if (event.keyCode == ChainJam.PLAYER2_ACTION1)
			{
				if (getPhase(1) != 3)
					return;
					
				actionButton(0, 1);
			}
			else if (event.keyCode == ChainJam.PLAYER2_ACTION2)
			{
				if (getPhase(1) != 4)
					return;
					
				actionButton(1, 1);
			}
			// PLAYER3
			else if (event.keyCode == ChainJam.PLAYER3_UP)
			{
				if (getPhase(2) > 1)
					return;
					
				directions[2] = -1;
			}
			else if (event.keyCode == ChainJam.PLAYER3_DOWN)
			{
				if (getPhase(2) > 1)
					return;
				
				directions[2] = 1;
			}
			else if (event.keyCode == ChainJam.PLAYER3_LEFT)
			{
				if (getPhase(2) != 2)
					return;
					
				directions[2] = -1;
			}
			else if (event.keyCode == ChainJam.PLAYER3_RIGHT)
			{
				if (getPhase(2) != 2)
					return;
					
				directions[2] = 1;
			}
			else if (event.keyCode == ChainJam.PLAYER3_ACTION1)
			{
				if (getPhase(2) != 3)
					return;
					
				actionButton(0, 2);
			}
			else if (event.keyCode == ChainJam.PLAYER3_ACTION2)
			{
				if (getPhase(2) != 4)
					return;
					
				actionButton(1, 2);
			}
			// PLAYER4
			else if (event.keyCode == ChainJam.PLAYER4_UP)
			{
				if (getPhase(3) > 1)
					return;
					
				directions[3] = -1;
			}
			else if (event.keyCode == ChainJam.PLAYER4_DOWN)
			{
				if (getPhase(3) > 1)
					return;
				
				directions[3] = 1;
			}
			else if (event.keyCode == ChainJam.PLAYER4_LEFT)
			{
				if (getPhase(3) != 2)
					return;
					
				directions[3] = -1;
			}
			else if (event.keyCode == ChainJam.PLAYER4_RIGHT)
			{
				if (getPhase(3) != 2)
					return;
					
				directions[3] = 1;
			}
			else if (event.keyCode == ChainJam.PLAYER4_ACTION1)
			{
				if (getPhase(3) != 3)
					return;
					
				actionButton(0, 3);
			}
			else if (event.keyCode == ChainJam.PLAYER4_ACTION2)
			{
				if (getPhase(3) != 4)
					return;
					
				actionButton(1, 3);
			}
		}
		
		private function getPhase(player:int):int
		{
			if (MustachesEnergys[player].partH1 > 0)
				return 0;
			else if	(MustachesEnergys[player].partH2 > 0)
				return 1;
			else if (MustachesEnergys[player].partV > 0)
				return 2;
			else if (MustachesEnergys[player].partL > 0)
				return 3;
			else if (MustachesEnergys[player].partR > 0)
				return 4;
			else
				return 5;
		}
		
		private function updateScores()
		{
			var maxScore:int = 5 * MustacheEnergy.maxEnergy;
			
			for (var i:int = 0; i < 4; i++)
			{
				scores[i] = maxScore - MustachesEnergys[i].partH1 - MustachesEnergys[i].partH2 - MustachesEnergys[i].partV - MustachesEnergys[i].partL - MustachesEnergys[i].partR;
			}
		}
		
		private function updatePlayer(player:int, elapsedTime:int):void
		{
			var phase:int = getPhase(player);
			
			switch (phase)
			{
				case 0:
					if (directions[player] != 0)
					{	
						combs[player].getChildAt(0).y += elapsedTime / 1000 * SPEED_Y * directions[player];
						if (combs[player].getChildAt(0).y > COMBH_EPOSITION_Y)
						{
							combs[player].getChildAt(0).y = COMBH_EPOSITION_Y;
							directions[player] = 0;
						}
						else if (combs[player].getChildAt(0).y < COMBH_SPOSITION_Y)
						{
							combs[player].getChildAt(0).y = COMBH_SPOSITION_Y;
							directions[player] = 0;
						}
						MustachesEnergys[player].partH1 -= elapsedTime / 1000 * ENERGY_SPEEDX;
						if ((directions[player] > 0 && combs[player].getChildAt(0).y > (COMBH_EPOSITION_Y - (COMBH_EPOSITION_Y - COMBH_SPOSITION_Y) / 10))
							&& (directions[player] < 0 && combs[player].getChildAt(0).y < (COMBH_SPOSITION_Y + (COMBH_EPOSITION_Y - COMBH_SPOSITION_Y) / 10)))
							MustachesEnergys[player].partH1 -= elapsedTime / 1000 * ENERGY_SPEEDX * 0.5;
						if (MustachesEnergys[player].partH1 <= MustacheEnergy.maxEnergy / 2)
							dusts[player].getChildAt(0).visible = false;
						if (MustachesEnergys[player].partH1 <= 0)
						{
							MustachesEnergys[player].partH1 = 0;
							dusts[player].getChildAt(1).visible = false;
							changePhaseTo(player, 1);
						}
					}
					break;
					
				case 1:
					if (directions[player] != 0)
					{	
						combs[player].getChildAt(0).y += elapsedTime / 1000 * SPEED_Y * directions[player];
						if (combs[player].getChildAt(0).y > COMBH_EPOSITION_Y)
						{
							combs[player].getChildAt(0).y = COMBH_EPOSITION_Y;
							directions[player] = 0;
						}
						else if (combs[player].getChildAt(0).y < COMBH_SPOSITION_Y)
						{
							combs[player].getChildAt(0).y = COMBH_SPOSITION_Y;
							directions[player] = 0;
						}
						MustachesEnergys[player].partH2 -= elapsedTime / 1000 * ENERGY_SPEEDX;
						if ((directions[player] > 0 && combs[player].getChildAt(0).y > (COMBH_EPOSITION_Y - (COMBH_EPOSITION_Y - COMBH_SPOSITION_Y) / 10))
							&& (directions[player] < 0 && combs[player].getChildAt(0).y < (COMBH_SPOSITION_Y + (COMBH_EPOSITION_Y - COMBH_SPOSITION_Y) / 10)))
							MustachesEnergys[player].partH2 -= elapsedTime / 1000 * ENERGY_SPEEDX * 0.5;
						if (MustachesEnergys[player].partH2 <= MustacheEnergy.maxEnergy / 2)
							dusts[player].getChildAt(2).visible = false;
						if (MustachesEnergys[player].partH2 <= 0)
						{
							dusts[player].getChildAt(3).visible = false;
							MustachesEnergys[player].partH2 = 0;
							changePhaseTo(player, 2);
						}
					}
					break;
					
				case 2:
					if (directions[player] != 0)
					{	
						combs[player].getChildAt(0).x += elapsedTime / 1000 * SPEED_X * directions[player];
						if (combs[player].getChildAt(0).x > COMBV_EPOSITION_X)
						{
							combs[player].getChildAt(0).x = COMBV_EPOSITION_X;
							directions[player] = 0;
						}
						else if (combs[player].getChildAt(0).x < COMBV_SPOSITION_X)
						{
							combs[player].getChildAt(0).x = COMBV_SPOSITION_X;
							directions[player] = 0;
						}
						MustachesEnergys[player].partV -= elapsedTime / 1000 * ENERGY_SPEEDY;
						if ((directions[player] > 0 && combs[player].getChildAt(0).x > (COMBV_EPOSITION_X - (COMBV_EPOSITION_X - COMBV_SPOSITION_X) / 10))
							&& (directions[player] < 0 && combs[player].getChildAt(0).x < (COMBV_SPOSITION_X + (COMBV_EPOSITION_X - COMBV_EPOSITION_X) / 10)))
							MustachesEnergys[player].partV -= elapsedTime / 1000 * ENERGY_SPEEDX * 0.5;
						if (MustachesEnergys[player].partV <= 0)
						{
							dusts[player].getChildAt(4).visible = false;
							MustachesEnergys[player].partV = 0;
							changePhaseTo(player, 3);
						}
					}
					break;
					
				case 3:
					break;
					
				case 4:
					break;
					
			}
		}
		
		private function actionButton(button:int, player:int):void
		{
			var phase:int = getPhase(player);
			
			if (phase == 3 && button == 0)
			{
				buttons1[player].getChildAt(1).visible = !buttons1[player].getChildAt(1).visible;
				
				if (hands[player].getChildAt(0).visible == true)
				{
					hands[player].getChildAt(0).visible = false;
					hands[player].getChildAt(1).visible = true;
				}
				else
				{
					hands[player].getChildAt(0).visible = true;
					hands[player].getChildAt(1).visible = false;
				}
				MustachesEnergys[player].partL -= ENERGY_SPEEDB;
				if (MustachesEnergys[player].partL < MustacheEnergy.maxEnergy / 2)
					dusts[player].getChildAt(5).visible = false;
				if (MustachesEnergys[player].partL <= 0)
				{
					dusts[player].getChildAt(6).visible = false;
					MustachesEnergys[player].partL = 0;
					changePhaseTo(player,4);
				}
			}
			else if (phase == 4 && button == 1)
			{
				buttons2[player].getChildAt(1).visible = !buttons2[player].getChildAt(1).visible;
				
				if (hands[player].getChildAt(0).visible == true)
				{
					hands[player].getChildAt(0).visible = false;
					hands[player].getChildAt(1).visible = true;
				}
				else
				{
					hands[player].getChildAt(0).visible = true;
					hands[player].getChildAt(1).visible = false;
				}
				MustachesEnergys[player].partR -= ENERGY_SPEEDB;
				if (MustachesEnergys[player].partR < MustacheEnergy.maxEnergy / 2)
					dusts[player].getChildAt(7).visible = false;
				if (MustachesEnergys[player].partR <= 0)
				{
					dusts[player].getChildAt(8).visible = false;
					MustachesEnergys[player].partR = 0;
					endGame();
				}
			}
		}
		private function changePhaseTo(player:int, phase:int):void
		{
			directions[player] = 0;
			switch (phase)
			{
				case 1:
					combs[player].getChildAt(0).y = COMBH_SPOSITION_Y;
					combs[player].getChildAt(0).x = COMBH_POSITION_X2;
					break;
				case 2:
					combs[player].getChildAt(0).rotation = 90;
					combs[player].getChildAt(0).x = COMBV_SPOSITION_X;
					buttons1[player].getChildAt(2).visible = false;
					buttons1[player].getChildAt(3).visible = false;
					buttons2[player].getChildAt(2).visible = false;
					buttons2[player].getChildAt(3).visible = false;
					buttons1[player].getChildAt(4).visible = true;
					buttons1[player].getChildAt(5).visible = false;
					buttons2[player].getChildAt(4).visible = true;
					buttons2[player].getChildAt(5).visible = true;
					break;
				case 3:
					buttons1[player].getChildAt(4).visible = false;
					buttons1[player].getChildAt(5).visible = false;
					buttons2[player].getChildAt(4).visible = false;
					buttons2[player].getChildAt(5).visible = false;
					buttons1[player].getChildAt(1).visible = true;
					buttons1[player].getChildAt(0).visible = true;
					combs[player].getChildAt(0).visible = false;
					hands[player].getChildAt(0).visible = true;
					hands[player].getChildAt(0).scaleX *= -1;
					hands[player].getChildAt(1).scaleX *= -1;
					break;
				case 4:
					buttons1[player].getChildAt(0).visible = false;
					buttons1[player].getChildAt(1).visible = false;
					buttons2[player].getChildAt(1).visible = true;
					buttons2[player].getChildAt(0).visible = true;
					hands[player].getChildAt(0).scaleX *= -1;
					hands[player].getChildAt(1).scaleX *= -1;
					hands[player].getChildAt(0).x = HAND_POSITION_X2;
					hands[player].getChildAt(1).x = HAND_POSITION_X2;
					break;
			}
		}
		
		private function endGame()
		{
			if (ended)
				return;
			ended = true;
			
			var index:int = -1;
			var maxScore:int = -1;
			var pointsGiven:Array = [4, 2, 1, 0];
			var j:int = 0;
			
			while (scores.length > 0)
			{
				maxScore = -1;
				for (var i:int = 0; i < scores.length; i++)
				{
						if (scores[i] > maxScore)
						{
							maxScore = scores[i];
							index = i;
						}
				}
				ChainJam.addPoints(index + 1, pointsGiven[j++]);
				scores.splice(index, 1);
			}
			ChainJam.endGame();
		}
	}
}