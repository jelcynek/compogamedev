using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace TombRajder
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        Texture2D gracz;
        Texture2D pixel;
        Texture2D wrog;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Rectangle graczRect;
        Rectangle cipa;
        Rectangle laser;

        int score;

        Random rnd;

        TimeSpan shoot = TimeSpan.FromMilliseconds(500);
        TimeSpan lastShoot;

        float enemyTime = 0.5f;
        float currentTime;
        int cipaSpeed = 1;

        TimeSpan shootTime = TimeSpan.FromMilliseconds(500);

        SpriteFont font;

        KeyboardState currentKeyboardstate;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
             rnd = new Random();
            graczRect = new Rectangle(400, 440, 40, 30);
            lastShoot = new TimeSpan(0);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            gracz = Content.Load<Texture2D>("gracz");
            wrog = Content.Load<Texture2D>("wrog");
            pixel = Content.Load<Texture2D>("pixel");

            font = Content.Load<SpriteFont>("font");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            currentKeyboardstate = Keyboard.GetState();

            laser = new Rectangle(laser.X, laser.Y - 3, laser.Width, laser.Height);
            //if (gameTime.ElapsedGameTime > enemyTime)
           //     ;
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (cipa == null || cipa.Y > graphics.GraphicsDevice.Viewport.Height || cipa.Y < -1000)
            {
                cipa = new Rectangle(rnd.Next(graphics.GraphicsDevice.Viewport.Width), 0, wrog.Width, wrog.Height);
            }

            cipa = new Rectangle(cipa.X, cipa.Y + cipaSpeed, wrog.Width, wrog.Height);
            laser = new Rectangle(laser.X, laser.Y - 1, 10, 5);

            // TODO: Add your update logic here
            if(currentKeyboardstate.IsKeyDown(Keys.Left))
                graczRect.X += -3;
            if (currentKeyboardstate.IsKeyDown(Keys.Right))
                graczRect.X += 3;
             if (currentKeyboardstate.IsKeyDown(Keys.Space) && gameTime.TotalGameTime - lastShoot > shootTime)  
            {
                lastShoot = gameTime.ElapsedGameTime;
                laser = new Rectangle(graczRect.Center.X, graczRect.Top, 10, 5);
            }
             UpdateCollision();

            base.Update(gameTime);
        }

        private void UpdateCollision()
        {
            if (laser.Intersects(cipa))
            {
                score += 100;
                cipa.Y = -1100;
                if ((score / 1000) > 0)
                    cipaSpeed += (score / 1000) * 1;
            }

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            spriteBatch.Draw(gracz, graczRect, Color.White);
            spriteBatch.Draw(pixel, laser, Color.White);
            spriteBatch.Draw(wrog, cipa, Color.White);
            spriteBatch.DrawString(font, "Score: " + score, Vector2.Zero, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}