﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        bool end = false;

        const float PlayerSpeed = 700.0f;

        const int msSpawnStart = 1000;
        const int msSpawnEnd = 10;
        const int msCooldown = 100;
        const int spawnDifPerSec = 25;

        const int timeToLiveTriangle = 5000;
        const float maxSizeTriangle = 0.15f;
        const int hpdec = 75;
        const int timeToShowChmuraMs = 500;

        float msSpawn = msSpawnStart;

        Texture2D diabeltextura;
        Texture2D wieza;
        Texture2D trojkat;
        Texture2D wybuch;
        Texture2D pentagram;

        SpriteFont font;
        SpriteFont duza;

        Player player;

        KeyboardState currentState;
        KeyboardState previousState;

        SoundEffect soundwybuch;
        SoundEffect soundtrojkat;
        SoundEffect soundDeath;
        SoundEffect soundEnd;
        SoundEffect soundError;

        List<Diabeloooo> diabelki = new List<Diabeloooo>();
        List<Triangle> moc = new List<Triangle>();

        int domekhp = 1000;
        int punkty = 0;
        
        TimeSpan spawnTimer = TimeSpan.Zero;
        TimeSpan cooldownTimer = TimeSpan.Zero;
        TimeSpan liveTimeChmura = TimeSpan.Zero;

        float scaleChmura = 1.0f;

        Random rnd = new Random();

        Rectangle forbidden = new Rectangle(380, 300, 200, 120);

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferredBackBufferWidth = 1024;

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            player = new Player(Content.Load<Texture2D>("path3819"), new Vector2(500,340), Vector2.Zero);
            diabeltextura = Content.Load<Texture2D>("diabel");
            wieza = Content.Load<Texture2D>("wieza");
            trojkat = Content.Load<Texture2D>("trojkat");
            font = Content.Load<SpriteFont>("font");
            wybuch = Content.Load<Texture2D>("wybuch");
            pentagram = Content.Load<Texture2D>("pentagram");
            duza = Content.Load<SpriteFont>("duza");
            soundwybuch = Content.Load<SoundEffect>("explosion");
            soundtrojkat = Content.Load<SoundEffect>("flash");
            soundEnd = Content.Load<SoundEffect>("end");
            soundDeath = Content.Load<SoundEffect>("death");
            soundError = Content.Load<SoundEffect>("error");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            previousState = currentState;
            currentState = Keyboard.GetState();

            msSpawn -= (float)(gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f * spawnDifPerSec);
            if (msSpawn < msSpawnEnd)
                msSpawn = msSpawnEnd;

            if (!end)
            {
                if (cooldownTimer.TotalMilliseconds > 0)
                {
                    cooldownTimer -= gameTime.ElapsedGameTime;
                    if (cooldownTimer.TotalMilliseconds <= 0)
                        cooldownTimer = TimeSpan.Zero;
                }

                if (liveTimeChmura.TotalMilliseconds > 0)
                {
                    liveTimeChmura -= gameTime.ElapsedGameTime;
                    if (liveTimeChmura.TotalMilliseconds <= 0)
                        liveTimeChmura = TimeSpan.Zero;
                }
                spawnTimer += gameTime.ElapsedGameTime;
                if (spawnTimer.TotalMilliseconds > msSpawn)
                {
                    spawnTimer = TimeSpan.Zero;

                    bool running = true;
                    Vector2 devilpos, debiltar;
                    do
                    {
                        devilpos = new Vector2(rnd.Next(3 * graphics.PreferredBackBufferWidth) - graphics.PreferredBackBufferWidth,
                                                            rnd.Next(3 * graphics.PreferredBackBufferHeight) - graphics.PreferredBackBufferHeight);

                        debiltar = new Vector2(graphics.PreferredBackBufferWidth / 2 - 20, graphics.PreferredBackBufferHeight / 2);
                        if ((debiltar - devilpos).Length() > (graphics.PreferredBackBufferWidth / 2)) running = false;
                    } while (running);
                    diabelki.Add(new Diabeloooo(diabeltextura
                                            , devilpos, debiltar
                                            , rnd.Next(100, 500)));
                }

                if (currentState.IsKeyDown(Keys.Up))
                    player.speed.Y = -PlayerSpeed;
                else if (currentState.IsKeyDown(Keys.Down))
                    player.speed.Y = PlayerSpeed;
                else
                    player.speed.Y = 0;

                if (currentState.IsKeyDown(Keys.Left))
                    player.speed.X = -PlayerSpeed;
                else if (currentState.IsKeyDown(Keys.Right))
                    player.speed.X = PlayerSpeed;
                else
                    player.speed.X = 0;

                if (Pressed(Keys.Space))
                {
                    if (cooldownTimer == TimeSpan.Zero && !forbidden.Contains(new Point((int)player.position.X, (int)player.position.Y)))
                    {
                        cooldownTimer = TimeSpan.FromMilliseconds(msCooldown);
                        moc.Add(new Triangle(trojkat, timeToLiveTriangle, maxSizeTriangle, new Vector2(player.position.X + player.texture.Width / 2,
                                                                                                       player.position.Y + player.texture.Height / 2)));
                        soundtrojkat.Play();

                    }
                    else
                        soundError.Play();
                }

                player.Update(gameTime);

                for (int i = 0; i < diabelki.Count; i++)
                {
                    if (diabelki[i].Update(gameTime))
                    {
                        diabelki.RemoveAt(i);
                        i--;
                        domekhp -= hpdec;

                        if (domekhp <= 0)
                        {
                            soundEnd.Play();
                            end = true;
                        }
                        soundwybuch.Play();
                        liveTimeChmura = TimeSpan.FromMilliseconds(timeToShowChmuraMs);
                        continue;
                    }
                }

                for (int i = 0; i < moc.Count; i++)
                {
                    if (moc[i].Update(gameTime))
                    {
                        moc.RemoveAt(i);
                        i--;
                        continue;
                    }
                }

                foreach (Triangle trianglemocy in moc)
                {
                    for (int i = 0; i < diabelki.Count; i++)
                    {
                        if (trianglemocy.Collide(diabelki[i].boundingBox))
                        {
                            soundDeath.Play();
                            diabelki.RemoveAt(i);
                            punkty += 100;
                            i--;
                            continue;
                        }
                    }
                }
                // TODO: Add your update logic here 
            }
            else
            {
                if (Pressed(Keys.Enter))
                    this.Exit();
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
            // TODO: Add your drawing code here
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
            spriteBatch.Draw(pentagram, Vector2.Zero, new Color(1.0f, 1.0f, 1.0f, 1.0f - domekhp/1000.0f));

            spriteBatch.Draw(wieza, new Vector2(350, 200), new Color(1.0f, 1.0f, 1.0f, (domekhp + 200)/1200.0f));

            if (liveTimeChmura.TotalMilliseconds > 0)
            {
                spriteBatch.Draw(wybuch, new Vector2(470, 380), null, Color.White, 0.0f, Vector2.Zero, (float)(1.5 - liveTimeChmura.TotalMilliseconds / timeToLiveTriangle) * 1.5f, SpriteEffects.None, 0);
            }

            player.Draw(spriteBatch);
            foreach (Triangle trojkat in moc)
                trojkat.Draw(spriteBatch);

            foreach (Diabeloooo diabel in diabelki)
                diabel.Draw(spriteBatch);

            spriteBatch.DrawString(font, "Punkty: " + punkty, new Vector2(10, 10), Color.Red);
            spriteBatch.DrawString(font, "HP: " + domekhp, new Vector2(10, 40), Color.Green);
            if (end)
            {
                spriteBatch.DrawString(duza, "hooj i koniec, ciota max!", new Vector2(100, 200), Color.PeachPuff);

                spriteBatch.DrawString(duza, "ENTER aBy EXITffsdf", new Vector2(100, 300), Color.Ivory);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }

        private bool Pressed(Keys key)
        {
            return (currentState.IsKeyDown(key) && previousState.IsKeyUp(key));
        }

        private bool Released(Keys key)
        {
            return (currentState.IsKeyUp(key) && previousState.IsKeyDown(key));

        }
    }
}
