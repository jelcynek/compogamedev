﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    class Player
    {
        public Texture2D texture;
        public Vector2 position;
        public Vector2 speed;

        public Player(Texture2D texture, Vector2 position, Vector2 speed)
        {
            this.texture = texture;
            this.position = position;
            this.speed = speed;
        }

        public void Update(GameTime gameTime)
        {
            double secFactor = gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0;
            position.X += (float)(speed.X * secFactor);
            position.Y += (float)(speed.Y * secFactor);


        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
