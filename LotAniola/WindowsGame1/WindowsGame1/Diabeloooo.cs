﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    class Diabeloooo
    {
        public Texture2D texture;
        public Vector2 position;
        Vector2 speed;
        public Vector2 target;
        public float scale = 1;

        public Rectangle boundingBox
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, (int)texture.Width, (int)texture.Height);
            }
        }

        public Diabeloooo(Texture2D texture, Vector2 position, Vector2 target, float scale)
        {
            this.texture = texture;
            this.position = position;
            this.target = target;
            this.scale = scale;
            AdjustSpeed();
        }

        public void AdjustSpeed()
        {
            speed.X = scale * Math.Sign(target.X - position.X);
            speed.Y = scale * Math.Sign(target.Y - position.Y);
        }

        public bool Update(GameTime gameTime)
        {
            double secFactor = gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0;
            AdjustSpeed();

            position.X += (float)(speed.X * secFactor);
            position.Y += (float)(speed.Y * secFactor);

            if (Math.Abs(position.X - target.X) < 5.0 && Math.Abs(position.Y - target.Y) < 5.0)
                return true;

            return false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
