﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    class Triangle
    {
        Texture2D triangle;
        int timeToLiveMS;
        float currentSize = 0.01f;
        float growSpeedOnSec = 1.0f;
        float maxSize;
        TimeSpan liveTime = TimeSpan.Zero;
        Vector2 position;
        float rotate = 0.0f;
        double rotateSpeed = Math.PI;

        public Triangle(Texture2D triangle, int timeToLiveMS, float maxSize, Vector2 position)
        {
            this.triangle = triangle;
            this.timeToLiveMS = timeToLiveMS;
            this.maxSize = maxSize;
            this.position = position;
        }

        public bool Update(GameTime gameTime)
        {
            double scaleTime = gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0;
            liveTime += gameTime.ElapsedGameTime;
            
            if (liveTime.TotalMilliseconds > timeToLiveMS)
                return true;

            currentSize += (float)(scaleTime * growSpeedOnSec);
            if (currentSize > maxSize)
                currentSize = maxSize;

            rotate += (float)(scaleTime * rotateSpeed);
            float dwapi = (float)(2 * Math.PI);
            if (rotate > dwapi)
                rotate -= dwapi;

            return false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Vector2 originVect =  new Vector2(triangle.Width / 2, triangle.Height / 2);
            spriteBatch.Draw(triangle, position, null, Color.Yellow, rotate * 0.8f, originVect, currentSize * 1.0f, SpriteEffects.None, 0);
            spriteBatch.Draw(triangle, position, null, Color.OrangeRed, rotate * (-1.0f), originVect, currentSize * 0.8f, SpriteEffects.None, 0);
            spriteBatch.Draw(triangle, position, null, Color.DarkMagenta, rotate * 1.2f, originVect, currentSize * 0.5f, SpriteEffects.None, 0);
        }

        public bool Collide(Rectangle boundingBox)
        {
            Rectangle box = new Rectangle((int)position.X, 
                                          (int)position.Y, 
                                          (int)(triangle.Width * currentSize * 0.8f), 
                                          (int)(triangle.Height * currentSize * 0.8f));
            return box.Intersects(boundingBox);
        }


    }
}
