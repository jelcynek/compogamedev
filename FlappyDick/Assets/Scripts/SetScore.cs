﻿using UnityEngine;
using System.Collections;

public class SetScore : MonoBehaviour {

	public GameObject digit1;
	public GameObject digit10;
	public GameObject digit100;

	public Sprite[] digitSprites;
	public bool highScore = false;

	private int score1 = 0;
	private int score10 = 0;
	private int score100 = 0;
	private AudioSource[] audioSources;

	// Use this for initialization
	void Start () 
	{
		if (highScore)
			LoadScore();
		ScoreUpdate();
		audioSources = GetComponents<AudioSource>();
	}

	private void LoadScore()
	{
		int loadHighScore = PlayerPrefs.GetInt("highscore");
		if (loadHighScore > 0 && loadHighScore < 1000)
			Set(loadHighScore);
	}

	public void SaveScore()
	{	
		int loadHighScore = PlayerPrefs.GetInt("highscore");
		int currentScore = score1 + score10 * 10 + score100 * 100;

		if (currentScore > loadHighScore)
			PlayerPrefs.SetInt("highscore", currentScore);
	}

	public void ScoreUp()
	{
		audioSources[0].Play();
		score1++;
		if (score1 > 9)
		{
			score1 = 0;
			score10++;
			audioSources[1].Play();
			if (score10 > 9)
			{
				score10 = 0;
				score100++;
			}
		}
		ScoreUpdate();
	}

	public void Set(int score)
	{
		score1 = score % 10;
		score /= 10;
		score10 = score % 10;
		score /= 10;
		score100 = score % 10;
		ScoreUpdate();
	}

	void ScoreUpdate()
	{
		digit1.GetComponent<SpriteRenderer>().sprite = digitSprites[score1];

		if (score10 > 0)
		{
			digit10.renderer.enabled = true;
			digit10.GetComponent<SpriteRenderer>().sprite = digitSprites[score10];
		}
		else
		{
			digit10.renderer.enabled = false;
		}

		if (score100 > 0)
		{
			digit100.renderer.enabled = true;
			digit100.GetComponent<SpriteRenderer>().sprite = digitSprites[score100];
		}
		else
		{
			digit100.renderer.enabled = false;
		}
	}
}
