﻿using UnityEngine;
using System.Collections;

public class ScrollMaterialSin : MonoBehaviour {

	public float speed = 2.5f;
	public float scalex = 1.2f;
	public float scaley = 0.7f;

	private Vector3 startPosition;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float f = Mathf.Repeat(Time.time * speed, Mathf.PI * 2.0f);
		Vector3 offset = new Vector3(-(f / 2.0f) * scalex, Mathf.Sin(f) * scaley, 0.0f);
		offset /= 2.0f;
		transform.position = startPosition + offset;
	}
}
