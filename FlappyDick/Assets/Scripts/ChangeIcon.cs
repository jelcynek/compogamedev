﻿using UnityEngine;
using System.Collections;

public class ChangeIcon : MonoBehaviour {

	public Sprite[] spriteList;

	private int currentSprite = 0;

	// Use this for initialization
	void Start () {
		GetComponent<SpriteRenderer>().sprite = spriteList[currentSprite];
	}
	
	void NextIcon()
	{
		currentSprite++;
		if (currentSprite >= spriteList.Length)
			currentSprite = 0;

		GetComponent<SpriteRenderer>().sprite = spriteList[currentSprite];
	}
}
