﻿using UnityEngine;
using System.Collections;

public class ChangeFace : MonoBehaviour {

	public Sprite changeFaceSprite;

	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer spriteRenderer in renderers)
		{
			spriteRenderer.sprite = changeFaceSprite;
		}
	}
}
