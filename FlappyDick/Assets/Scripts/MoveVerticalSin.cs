﻿using UnityEngine;
using System.Collections;

public class MoveVerticalSin : MonoBehaviour {

	public float speed = 3.0f;
	public float maxY = 4.2f;
	public float minY = 1.0f;
	public float minDif = 0.5f;

	private float highY = 0.0f;
	private float lowY = 0.0f;

	// Use this for initialization
	void Start () 
	{
		highY = Random.Range(minY, maxY);
		lowY = Random.Range(minY, highY - minDif);
	}
	
	// Update is called once per frame
	void Update () 
	{
		float size = highY - lowY;
		float f = Mathf.Repeat(Time.time * speed, Mathf.PI * 2);
		Vector3 moveY = new Vector3(transform.position.x, (size / 2.0f) * Mathf.Sin(f) + 1.0f + lowY, 0.0f);
		transform.position = moveY;
	}
}
