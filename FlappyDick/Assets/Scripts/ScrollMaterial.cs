﻿using UnityEngine;
using System.Collections;

public class ScrollMaterial : MonoBehaviour {

	public float speed = 5.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 offset = new Vector2(Mathf.Repeat(Time.time * speed, 1.0f), 0f);
		renderer.material.mainTextureOffset = offset;
	}
}
