﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ScrollBackground : MonoBehaviour {

	public float speed = 3.0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Translate(new Vector3(-speed * Time.deltaTime, 0 , 0));

		Component[] renderers = this.transform.GetComponentsInChildren(typeof(Renderer));

		for (int i = 0; i < renderers.Length; i++)
		{
			if (!IsSeenFrom(Camera.main, renderers[i].renderer))
			{
				Destroy(this.gameObject);
				break;
			}
		}
	}

	bool IsSeenFrom(Camera camera, Renderer renderer)
	{
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
		return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
	}
}
