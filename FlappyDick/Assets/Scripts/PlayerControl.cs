using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	public GameObject gui;
	public Transform waterBound;

	private bool previousMouseLeft = false;
	private bool addForce = false;

	private float velocityY;
	
	public float force = 5.0f;
	public float rotateFactor = 1.0f;
	public float jumpVelocity = -1.0f;

	public Transform soundButton;
	public Transform musicButton;

	private AudioSource[] audioSources;

	private bool died = false;
	private bool splashPlayed = false;

	// Use this for initialization
	void Start () {
		Time.timeScale = 0.0f;
		Vector3 startVelocity = new Vector3(0.0f, 0.5f, 0.0f);
		rigidbody2D.velocity = startVelocity;
		audioSources = GetComponents<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		velocityY = rigidbody2D.velocity.y;

		Vector3 eulerAngles = new Vector3(0.0f, 0.0f, Mathf.Clamp(velocityY * rotateFactor, -60.0f, 60.0f));
		transform.eulerAngles = eulerAngles;

		GetComponent<Animator>().SetFloat("VelocityY", rigidbody2D.velocity.y);

#if UNITY_WP8
		InputMobile();
#else
		InputDesktop();
#endif

		if (died)
		{
			if (!splashPlayed && transform.position.y < waterBound.position.y)
			{
				splashPlayed = true;
				audioSources[1].Play ();
			}
		}
	}

	void FixedUpdate() 
	{
		if (died) 
			return;

		if (addForce)
		{
			if (rigidbody2D.velocity.y <= jumpVelocity)
			{
				audioSources[0].Play ();
				rigidbody2D.AddForce(Vector2.up * force);
			}
		}
		addForce = false;
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		Die();
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		collision.collider2D.enabled = false;
		gui.SendMessage("ScoreUp");
	}

	void Die()
	{
		died = true;
		collider2D.enabled = false;
		Animator animator = GetComponent<Animator>();
		animator.SetTrigger("Die");
		rigidbody2D.AddForce(Vector2.up * 400.0f);
		audioSources[2].Play();
		gui.SendMessage("SaveScore");
	}

	void InputDesktop()
	{
		bool currentMouseLeft = Input.GetMouseButtonDown(0);
		
		if ((currentMouseLeft && !previousMouseLeft) || Input.GetButtonDown("Jump"))
		{
			Clicked();
		}
		previousMouseLeft = currentMouseLeft;
	}

	void InputMobile()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			if (Time.timeScale == 0.0f)
				Application.Quit();
			else
				Application.LoadLevel(0);
		}

		foreach (Touch touch in Input.touches)
		{
			if (touch.phase == TouchPhase.Began)
			{
				Clicked();
			}
		}
	}

	void Clicked()
	{
		if (died)
		{
			Application.LoadLevel(0);
		}
		else if (Time.timeScale == 0.0f)
		{
			Time.timeScale = 1.0f;
			GameObject.Find ("menu").SetActive(false);
			gui.SetActive(true);
			audioSources[3].Play();
		}
		else
		{
			addForce = true;
		}
	}
}
