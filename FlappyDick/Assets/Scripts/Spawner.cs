﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject prefab;
	public GameObject prefab2;
	public GameObject prefab3;
	public float spawnTime = 1.5f;
	public float kenneySpawn = 1.0f;

	public float maxY = 4.2f;
	public float minY = 1.0f;
	public int spawnPhase = 10;

	private float currentTime = 0.0f;
	private int spawn = 0;
	private bool spawnKenney = false;
	// Use this for initialization
	void Start () {
		Vector3 stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(0.95f * Screen.width, 0,0));
		transform.position = stageDimensions;
		Spawn();
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
	
		if (currentTime >= spawnTime)
		{
			currentTime = 0.0f;
			spawn++;
			Spawn();

			if (spawn == 15)
				spawnKenney = true;
		}
		else if (spawnKenney && currentTime >= kenneySpawn)
		{
			spawnKenney = false;
			SpawnKenney();
		}

	}

	void Spawn()
	{
		GameObject temp;

		if (spawn < spawnPhase || 
		    (spawn < spawnPhase*2 && (spawn % 2) == 1) )
		{
			temp = Instantiate(prefab) as GameObject;
		}
		else
		{
			temp = Instantiate(prefab2) as GameObject;
			float speedmodif = 1.0f + (Mathf.Clamp(spawn - 20, 0, spawn) / 10) * 0.25f;
			temp.GetComponent<MoveVerticalSin>().speed *= speedmodif;
		}
		float x = transform.position.x;
		float y = Random.Range (minY, maxY);
		Vector3 position = new Vector3(x, y, 0.0f);
		temp.transform.position = position;
	}

	void SpawnKenney()
	{
		GameObject temp;
		temp = Instantiate(prefab3) as GameObject;
		float x = transform.position.x;
		float y = -2.1f;
		Vector3 position = new Vector3(x, y, 0.0f);
		temp.transform.position = position;
	}
}
