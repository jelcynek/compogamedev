﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FancySniperExplosion
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        Texture2D window;
        Texture2D bandit;
        Texture2D banditShooting;
        Texture2D lady;

        SpriteBatch spriteBatch;
        Texture2D crosshair;
        Texture2D floorTexture;
        Entity player;
        SpriteFont font;
        List<Rectangle> platforms;
        KeyboardState currentState;
        KeyboardState previousState;
        MouseState currentMState;
        MouseState previousMState;
        List<GWindow> windowsList;
        TimeSpan lastWindow;
        TimeSpan newWindow;

        TimeSpan lastPlatform;
        TimeSpan newPlatform;

        int lives = 3;
        int windowSpeed = 10;
        int platformSpeed = 20;
        int score = 0;

        bool Start = false;

        Random rnd = new Random(DateTime.Now.Millisecond);

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Restart();
            base.Initialize();
        }

        public void Restart()
        {
            score = 0;
            lives = 3;
            platforms = new List<Rectangle>();
            platforms.Add(new Rectangle(-10, graphics.PreferredBackBufferHeight * 4 / 5, graphics.PreferredBackBufferWidth + 10, graphics.PreferredBackBufferHeight - (graphics.PreferredBackBufferHeight * 4 / 5)));
            windowsList = new List<GWindow>();
            currentState = Keyboard.GetState();
            currentMState = Mouse.GetState();

            lastWindow = TimeSpan.Zero;
            newWindow = new TimeSpan(0, 0, 0, 0, 2000);

            lastPlatform = TimeSpan.Zero;
            newPlatform = new TimeSpan(0, 0, 0, 0, 2000);
        }
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            player = new Entity();
            player.x = 200;
            player.y = 300;
            player.texture = Content.Load<Texture2D>("banan");
            // TODO: use this.Content to load your game content here
            crosshair = Content.Load<Texture2D>("crosshair");
            font = Content.Load<SpriteFont>("misunderstood");

            floorTexture = new Texture2D(GraphicsDevice, 1, 1);
            floorTexture.SetData(new Color[] { Color.White });

            window = Content.Load<Texture2D>("window");
            bandit = Content.Load<Texture2D>("bandit");
            banditShooting = Content.Load<Texture2D>("banditshooting");
            lady = Content.Load<Texture2D>("lady");

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            
            previousState = currentState;
            currentState = Keyboard.GetState();
            previousMState = currentMState;
            currentMState = Mouse.GetState();

            if (Start)
            {
                lastPlatform += gameTime.ElapsedGameTime;
                newPlatform = new TimeSpan(0, 0, 0, 0, 2000 + rnd.Next(-500, 500));
                if (lastPlatform > newPlatform)
                {
                    Rectangle tempRect = new Rectangle(graphics.PreferredBackBufferWidth,
                                                       rnd.Next(50, graphics.PreferredBackBufferHeight - 10),
                                                       rnd.Next(20, graphics.PreferredBackBufferWidth/2),
                                                       10);
                    platforms.Add(tempRect);
                    lastPlatform = TimeSpan.Zero;
                }

                for (int i = 0; i < platforms.Count; i++)
                {
                    platforms[i] = new Rectangle(platforms[i].X - (int)(platformSpeed * gameTime.ElapsedGameTime.Milliseconds / 100.0f),
                                                 platforms[i].Y, platforms[i].Width, platforms[i].Height);
                    if (platforms[i].X < -platforms[i].Width - 10)
                    {
                        platforms.RemoveAt(i);
                    }
                }

                lastWindow += gameTime.ElapsedGameTime;
                newWindow = new TimeSpan(0, 0, 0, 0, 2000 + rnd.Next(-500, 500));
                if (lastWindow > newWindow)
                {
                    GWindow tempWindow = new GWindow();
                    tempWindow.window = window;
                    tempWindow.lady = lady;
                    tempWindow.bandit = bandit;
                    tempWindow.banditShooting = banditShooting;
                    tempWindow.x = graphics.PreferredBackBufferWidth;
                    tempWindow.y = rnd.Next(0, graphics.PreferredBackBufferHeight - tempWindow.window.Height);
                    windowsList.Add(tempWindow);
                    lastWindow = TimeSpan.Zero;
                }

                for (int i = 0; i < windowsList.Count; i++)
                {
                    windowsList[i].x -= windowSpeed * gameTime.ElapsedGameTime.Milliseconds / 100.0f;
                    if (windowsList[i].x < -this.window.Width)
                    {
                        windowsList.RemoveAt(i);
                    }
                    else
                    {
                        if (windowsList[i].Update(gameTime))
                            lives--;
                    }
                }

                if (Entity.MouseKeyPressed(currentMState, previousMState))
                {
                    for (int i = 0; i < windowsList.Count; i++)
                    {
                        Rectangle tempRect = new Rectangle((int)windowsList[i].x + 8,
                                                           (int)windowsList[i].y + 10,
                                                           GWindow.WINDOWWIDTH - 16,
                                                           GWindow.WINDOWHEIGHT - 20);
                        if (tempRect.Contains(new Point(currentMState.X, currentMState.Y)))
                        {
                            if (windowsList[i].Kill() < 0)
                                lives--;
                            else
                                score += 1000;
                        }

                    }
                }

                player.OnKeyboard(currentState, previousState);
                // TODO: Add your update logic here
                player.Update(gameTime, platforms);


                if (lives < 0 || player.y < -50)
                    Start = false;
            }

            if (Entity.KeyPressed(currentState, previousState, Keys.Enter) && !Start)
            {
                Restart();
                Start = true;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            String status = "Score: " + score + " Lives: " + lives;
            String start = "Press ENTER to START";
 
            MouseState mouseState = Mouse.GetState();
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            foreach (GWindow window in windowsList)
                window.OnDraw(spriteBatch);
            foreach (Rectangle rect in platforms)
                spriteBatch.Draw(floorTexture, rect, Color.Blue);
            spriteBatch.Draw(player.texture, new Rectangle((int)player.x, (int)player.y, 25, player.texture.Height), new Rectangle(0,0,25,30), Color.White);
            spriteBatch.DrawString(font, status, Vector2.Zero, Color.Red);
            if (!Start)
                spriteBatch.DrawString(font, start, new Vector2(300, 200), Color.Red);
            spriteBatch.Draw(crosshair, new Rectangle(mouseState.X - crosshair.Width / 8, mouseState.Y - crosshair.Height / 8, crosshair.Width / 4, crosshair.Height / 4), Color.White);
            spriteBatch.End();
           
            base.Draw(gameTime);
        }
    }
}
