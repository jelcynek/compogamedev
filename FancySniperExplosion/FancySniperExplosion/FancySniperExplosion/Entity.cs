﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FancySniperExplosion
{
    class Entity
    {
        static float GRAVITY_ACC = 5;
        public float playerSpeedAcc = 10;
        public float jumpForce = -40;
        public float x { get; set; }
        public float y { get; set; }
        public Texture2D texture { get; set; }
        public float accX;
        public float accY;
        public float speedX;
        public float speedY;
        public float maxSpeedX = 10;
        public float maxSpeedY = 30;
        bool moveLeft, moveRight;
        bool jump = false;
        bool doubleJump = false;
        public Entity()
        {
            x = y = 0;
            accX = 0;
            accY = GRAVITY_ACC;
            speedX = speedY = 0;
            moveLeft = moveRight = false;
        }

        public void OnKeyboard(KeyboardState keyState, KeyboardState previousState)
        {
            if (KeyPressed(keyState, previousState, Keys.A))
                moveLeft = true;
            if (KeyPressed(keyState, previousState, Keys.D))
                moveRight = true;
            if (KeyPressed(keyState, previousState, Keys.W))
                Jump();

            if (KeyReleased(keyState, previousState, Keys.A))
                moveLeft = false;
            if (KeyReleased(keyState, previousState, Keys.D))
                moveRight = false;
        }

        public void Jump()
        {
            if (doubleJump)
                return;
            
            if (jump)
                doubleJump = true;

            jump = true;

            speedY = jumpForce;
        }

        public void StopMove()
        {
            if (speedX > 0)
                accX = -5;

            if (speedX < 0)
                accX = 5;

            if (speedX < 3.0f && speedX > -3.0f)
            {
                accX = 0;
                speedX = 0;
            }
        }

        public void Update(GameTime gameTime, List<Rectangle> collideBoxes)
        {
            float oldX = x;
            float oldY = y;

            if (moveLeft == false && moveRight == false)
                StopMove();

            if (moveLeft)
                accX = -playerSpeedAcc;
            if (moveRight)
                accX = playerSpeedAcc;

            speedX += accX * gameTime.ElapsedGameTime.Milliseconds / 100.0f;
            speedY += accY * gameTime.ElapsedGameTime.Milliseconds / 100.0f;

            speedX = MathHelper.Clamp(speedX, -maxSpeedX, maxSpeedX);
            speedY = MathHelper.Clamp(speedY, -maxSpeedY, maxSpeedY);

            y += speedY * gameTime.ElapsedGameTime.Milliseconds / 100.0f;
            if (y != oldY)
            {
                foreach (Rectangle rect in collideBoxes)
                {
                    if (Collide(rect))
                    {
                        y = oldY;
                        do
                        {
                            if (speedY > 0)
                                y++;
                            else
                                y--;
                        }
                        while (!Collide(rect));
                        if (speedY > 0)
                            y--;
                        else
                            y++;
                        jump = doubleJump = false;
                    }
                }
            }

            x += speedX * gameTime.ElapsedGameTime.Milliseconds / 100.0f;
            if (x != oldX)
            {
                foreach (Rectangle rect in collideBoxes)
                {
                    if (Collide(rect))
                    {
                        x = oldX;
                        do
                        {
                            if (speedX > 0)
                                x++;
                            else
                                x--;
                        }
                        while (!Collide(rect));
                        if (speedX > 0)
                            x--;
                        else
                            x++;
                    }
                }
            }
        }

        public bool Collide(Rectangle rect)
        {
            Rectangle playColBox = new Rectangle((int)x, (int)y, texture.Width, texture.Height);
            return playColBox.Intersects(rect);
        }

        public static bool KeyPressed(KeyboardState keyState, KeyboardState previousState, Keys key)
        {
            if (keyState.IsKeyDown(key) && previousState.IsKeyUp(key))
                return true;
            return false;
        }

        public static bool KeyReleased(KeyboardState keyState, KeyboardState previousState, Keys key)
        {
            if (keyState.IsKeyUp(key) && previousState.IsKeyDown(key))
                return true;
            return false;
        }

        public static bool MouseKeyPressed(MouseState mState, MouseState previousState)
        {
            if (mState.LeftButton == ButtonState.Pressed && previousState.LeftButton == ButtonState.Released)
                return true;

            return false;
        }
    }
}
