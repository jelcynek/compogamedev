﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FancySniperExplosion
{
    enum WindowState
    {
        FREE,
        BANDIT = 1,
        LADY = 2,
        BANDITSHOOTING,
        CLOSED
    }

    class GWindow
    {
        public static int WINDOWWIDTH = 110;
        public static int WINDOWHEIGHT = 130;

        public float x;
        public float y;
        WindowState windowState = WindowState.FREE;
        public Texture2D window;
        public Texture2D lady;
        public Texture2D bandit;
        public Texture2D banditShooting;
        TimeSpan openTime;
        TimeSpan shootTime;
        TimeSpan elapsedTime; 
        Random rnd;
        bool shooting = false;
        int shootCounter = 0;

        public GWindow()
        {
            rnd = new Random(DateTime.UtcNow.Millisecond);

            openTime = new TimeSpan(0,0,0,0,rnd.Next(1000, 3000));
            shootTime = new TimeSpan(0,0,0,0,rnd.Next(100,6000));
            elapsedTime = TimeSpan.Zero;
        }

        public bool Update(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime;

            if (windowState == WindowState.CLOSED)
                return false;

            if (windowState == WindowState.FREE)
            {
                if (elapsedTime > openTime)
                    OpenWindow();
            }

            if (windowState == WindowState.BANDIT)
            {
                if (!shooting && elapsedTime > shootTime)
                {
                    elapsedTime = TimeSpan.Zero;
                    shooting = true;
                }

                if (shooting)
                {
                    if (ShootBanditShoot(elapsedTime))
                    {
                        windowState = WindowState.BANDITSHOOTING;
                        elapsedTime = TimeSpan.Zero;
                        return true;
                    }
                }
            }

            if (windowState == WindowState.BANDITSHOOTING)
            {
                if (elapsedTime > new TimeSpan(0,0,0,0,500))
                    windowState = WindowState.CLOSED;
            }

            return false;
        }

        public void OpenWindow()
        {
            elapsedTime = TimeSpan.Zero;
            windowState = (WindowState)rnd.Next(1,3);   
        }

        public bool ShootBanditShoot(TimeSpan elapsedTime)
        {
            if (elapsedTime > new TimeSpan(0,0,1))
            {
                elapsedTime = TimeSpan.Zero;
                shootCounter++;
            }
            if (shootCounter >= 6)
                return true;

             return false;
        }

        public int Kill()
        {
            int value = 0;
            if (windowState == WindowState.CLOSED || windowState == WindowState.FREE)
                return 0;
            if (windowState == WindowState.LADY)
                value = -1;
            else
                value = 1;
            windowState = WindowState.CLOSED;
            return value;
        }

        public void OnDraw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(window, new Rectangle((int)x, (int)y, WINDOWWIDTH, WINDOWHEIGHT),    Color.White );

            int tX = (int)x + 8;
            int tY = (int)y + 10;
            int tWidth = WINDOWWIDTH - 16;
            int tHeight = WINDOWHEIGHT - 20;

            switch (windowState)
            {
                case WindowState.BANDIT:
                    Color color = ((shootCounter % 2) == 0 ? Color.White : Color.Red);
                    spriteBatch.Draw(bandit, new Rectangle(tX, tY, tWidth, tHeight), color);
                    break;

                case WindowState.BANDITSHOOTING:
                    spriteBatch.Draw(banditShooting, new Rectangle(tX, tY, tWidth, tHeight), Color.White);
                    break;

                case WindowState.LADY:
                    spriteBatch.Draw(lady, new Rectangle(tX, tY, tWidth, tHeight), Color.White);
                    break;

            }

        }

    }
}
